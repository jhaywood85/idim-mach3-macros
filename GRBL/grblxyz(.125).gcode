G38.2 Z-15 F100; Probe Down
G91 G0 Z1; Prepare for second probe
G38.2 Z-3 F50; Second Probe Down
G92 Z22; Zero at Plate height ##### Change me to your plate height (should be a positive)#####
G91 G0 Z3 F100; Move off the plate +3mm
G91 G0 X-30; Move -X 30mm
G91 G0 Z-6; Move -Z 6mm ##### Change me to the distance down you want to start you X probe at -3 so 6 will make you -3 down the plate side (should be a negative)#####
G38.2 X15 F100; Probe 15mm +X
G91 G0 X-1; Prepare for second probe
G38.2 X3 F50; Second probe 1mm +X
G92 ‭X-8.5875‬; Set X to - wall thickness - Radius of bit ##### Change me to the wall thikcness + 1/2 of the bit diameter (should be a negative)##### 
G91 G0 X-3 F100; Move off the plate 3mm
G91 G0 Z6; Move up +Z 6mm ##### Change me to the positive of distance you set to move down the plate wall above (should be a positive)#####
G91 G0 Y-30 X30; Move to next Probe position
G91 G0 Z-6; Move -Z 6mm ##### Change me to the distance down you want to start you X probe at -3 so 6 will make you -3 down the plate side (should be a negative)#####
G38.2 Y15 F100; Probe +Y 15mm
G91 G0 Y-1; Prepare for second probe
G38.2 Y3 F50; Second probe 1mm +Y
G92 Y-8.5875‬; Set Y to - wall thickness - Radius of bit ##### Change me to the wall thikcness + 1/2 of the bit diameter (should be a negative)##### 
G91 G0 Y-3 F100; Move off the plate 3mm
G91 G0 Z6; Move up +Z 6mm ##### Change me to the distance down you want to start you X probe at -3 so 6 will make you -3 down the plate side (should be a positive)#####
G90 G0 X0 Y0; Move to Zeros up 10mm for visual confirmation
